#!/bin/bash
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/tmp/install.log 2>&1
set -x

#curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
#curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list | sudo tee /etc/apt/sources.list.d/msprod.list
#apt-get update
#DEBIAN_FRONTEND=noninteractive ACCEPT_EULA=Y apt-get install -qqy mc vim-nox perl-doc libdbi-perl mssql-tools unixodbc-dev libdbd-odbc-perl
add-apt-repository ppa:greymd/tmux-xpanes
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -qqy git mc vim-nox tmux tmux-xpanes

# tmux
USERHOME=/home/vagrant
mkdir -p ${USERHOME}/.tmux/plugins
git clone https://github.com/tmux-plugins/tpm ${USERHOME}/.tmux/plugins/tpm
cp /vagrant/.tmux.conf ${USERHOME}/
chown -R vagrant:vagrant ${USERHOME}/.tmux*

cat <<-EOF > /tmp/tmux.sh
#!/bin/bash
tmux source ${USERHOME}/.tmux.conf
${USERHOME}/.tmux/plugins/tpm/bin/install_plugins
${USERHOME}/.tmux/plugins/tpm/bin/update_plugins all
EOF
chown vagrant:vagrant /tmp/tmux.sh
chmod +x /tmp/tmux.sh
su -l -c /tmp/tmux.sh vagrant
