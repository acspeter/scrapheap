#!/bin/bash
echo "Running master_install.sh"
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/tmp/master.log 2>&1
set -xueo pipefail

rm -f /vagrant/discovery.yaml

# Verify we have all the images
kubeadm config images pull

kubeadm init --apiserver-advertise-address 10.0.0.10 --token frlvfp.e18tfz5qpa85yghv --pod-network-cidr=10.244.0.0/16

#kubectl work for user
for USERNAME in vagrant root
do
	HOMEDIR=$(getent passwd ${USERNAME} | cut -d: -f6)
	mkdir -p "/${HOMEDIR}/.kube"
	cp -i /etc/kubernetes/admin.conf "/${HOMEDIR}/.kube/config"
	chown -R "${USERNAME}:${USERNAME}" "/${HOMEDIR}/.kube"
done

# get network interface name
NETIF=$(ip a sh to 10.0.0.0/24 | grep mtu | awk '{print $2}' | cut -d: -f1)
# install flannel
curl https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml | sed 's/- --kube-subnet-mgr/- --kube-subnet-mgr\n        - --iface='${NETIF}'/' > /root/kube-flannel.yml
#kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
kubectl apply -f /root/kube-flannel.yml

# fix kubelet config
NODEIP=$(ip a show to 10.0.0.0/24 | grep inet | awk '{print $2}' | cut -d/ -f1)
echo "KUBELET_EXTRA_ARGS=--node-ip=$NODEIP" >> /etc/default/kubelet
systemctl daemon-reload
systemctl restart kubelet.service

# export discovery.yml
kubectl -n kube-public get configmap cluster-info -o jsonpath='{.data.kubeconfig}' > /vagrant/discovery.yaml
