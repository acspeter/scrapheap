#!/bin/bash
echo "Running worker_install.sh"
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/tmp/worker.log 2>&1
set -xueo pipefail

#Kubeadm config
mkdir -p /etc/kubernetes/
cp /vagrant/discovery.yaml /etc/kubernetes/
kubeadm join --token frlvfp.e18tfz5qpa85yghv 10.0.0.10:6443 --discovery-token-unsafe-skip-ca-verification
# fix kubelet config
# get network interface name
NODEIP=$(ip a show to 10.0.0.0/24 | grep inet | awk '{print $2}' | cut -d/ -f1)
echo "KUBELET_EXTRA_ARGS=--node-ip=$NODEIP" >> /etc/default/kubelet
systemctl daemon-reload
systemctl restart kubelet.service
