#!/bin/bash
export GPG_TTY=$(tty)
/usr/bin/pkill -u "$USER" gpg-agent
if /usr/bin/pgrep -u "$USER" gpg-agent
then
	pkill -u "$USER" -9 gpg-agent
fi
/usr/bin/gpg-connect-agent updatestartuptty /bye
