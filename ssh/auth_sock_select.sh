#!/bin/bash
SOCKET_FILE=$(ls -l /tmp/ssh*/agent.* ${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh | awk  "\$3 ~ /${SUDO_USER:-$USER}/ {print}" | fzf --header='
Select SSH agent socket

' --reverse --preview='listkeys.sh {}' | awk '{print $NF}')
ln -sf ${SOCKET_FILE} ~/.ssh/ssh_auth_sock
